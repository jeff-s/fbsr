This is a simple project to compile a standalone JAR for https://github.com/demodude4u/Factorio-FBSR/

### Compiling

To build a standalone JAR:

- Clone this repo
- Run `git submodule update --init`
- Run `mvn package`

The standalone JAR can be found in `target/`

### Running

To run you need a minimal `config.json` in the current working directory:

```json
{
  "webapi": {
    "bind": "127.0.0.1",
    "port": 8080,
    "use-local-storage": true,
    "local-storage": "target/renders"
  },
  "factorio": "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Factorio"
}
```

If you are running this on a server, you can copy just the `data` folder from Factorio onto that server.  The
`"factorio"` config key should point at `data`'s parent directory (so if you copied to `/var/lib/factorio/data`, you
should put `/var/lib/factorio` in config)

Launch the JAR with `java -Djava.awt.headless=true -jar target/fbsr-1.1.0-20220402.jar`, which will start a service
that renders blueprints.

The easiest way to get images out of it is via the `return-single-image` flag.  Here's an example:

```
BLUEPRINT="0eJylksGKg0AMQH9lyXkqVcdxO8d+Qe/LUrQNbUCjzEx3FfHfO6M9LCsttL0lIe+RhAxQVhdsDbEDPQCyI0doQX/dkn7Pl7pEAzoWwEWNoMGZgm3bGLcqsXIgoG2sxxoOig70Ssp1lAnofZiqKBtHMcCRDB7mpkT8dyevufOlWy3c6RPudPNobrlwy9fc09zfAujQ8Hxr4iN2040tnbioAnXP7Po21MlhDdOINzh5B07fgeXzcNg+RPrPAwqoCt/sa1uPfOyIf8/ocwE/aOz8Op+xzOUmV3m8VpkaxysYZeco"
curl -X "POST" "http://localhost:8080/blueprint" \
  -H 'Content-Type: application/json' \
  -d '{"max-height":1200,"max-width":1200,"show-info-panels":false,"return-single-image":true,"blueprint":"$BLUEPRINT"}' \
  -o render.png
```

### Updating submodules

- Update [pom.xml](pom.xml)
- Run `git submodule foreach git pull --rebase origin`
- Run `git add .gitmodules submodules pom.xml`
- Commit & build

### Credits

This project combines three MIT-licensed projects, the licenses which you can see in their respective repos:

- https://github.com/demodude4u/Factorio-FBSR/
- https://github.com/demodude4u/Java-Factorio-Data-Wrapper
- https://github.com/demodude4u/Discord-Core-Bot-Apple

### License

This project is licensed under the terms of the MIT license.  See [LICENSE.txt](LICENSE.txt) for details.
