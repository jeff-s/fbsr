package com.factoriobin.fbsr;

import com.demod.factorio.FactorioData;
import com.demod.fbsr.app.StartAllServices;
import java.io.IOException;
import org.json.JSONException;

public class Main {
    public static void main(String[] args) throws JSONException, IOException {
        if (System.getenv("FBSR_SKIP_INITIAL_LOAD") == null) {
            //Force loading and caching Factorio data in memory on startup
            //This way the first blueprint we need to render in the future will be faster
            System.out.println("Loading mod data ...");
            FactorioData.getTable();
        }
        StartAllServices.main(args);
    }
}
